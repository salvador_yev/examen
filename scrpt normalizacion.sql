-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema escuela
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema escuela
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `escuela` DEFAULT CHARACTER SET utf8 ;
USE `escuela` ;

-- -----------------------------------------------------
-- Table `escuela`.`estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`estado` (
  `Id_Estado` INT(10) NOT NULL,
  `Clave_entidad` TINYINT NOT NULL,
  `Clave` VARCHAR(45) NOT NULL,
  `Entidad` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Id_Estado`))
ENGINE = InnoDB
AUTO_INCREMENT = 5715
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `escuela`.`municipio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`municipio` (
  `Id_municipio` INT(10) NOT NULL,
  `Clave_municipio` TINYINT NOT NULL,
  `Municipio` VARCHAR(45) NOT NULL,
  `estado_Id_Estado` INT(10) NOT NULL,
  PRIMARY KEY (`Id_municipio`, `estado_Id_Estado`),
  INDEX `fk_municipio_estado1_idx` (`estado_Id_Estado` ASC) VISIBLE,
  CONSTRAINT `fk_municipio_estado1`
    FOREIGN KEY (`estado_Id_Estado`)
    REFERENCES `escuela`.`estado` (`Id_Estado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `escuela`.`localidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`localidad` (
  `Id_Localidad` INT(10) NOT NULL,
  `Clave_localidad` TINYINT NOT NULL,
  `Localidad` VARCHAR(45) NOT NULL,
  `municipio_Id_municipio` INT(10) NOT NULL,
  PRIMARY KEY (`Id_Localidad`, `municipio_Id_municipio`),
  INDEX `fk_localidad_municipio1_idx` (`municipio_Id_municipio` ASC) VISIBLE,
  CONSTRAINT `fk_localidad_municipio1`
    FOREIGN KEY (`municipio_Id_municipio`)
    REFERENCES `escuela`.`municipio` (`Id_municipio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `escuela`.`escuela`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`escuela` (
  `Id_Escuela` INT(10) NOT NULL,
  `Centro_educativo` VARCHAR(45) NOT NULL,
  `Tipo_educativo` VARCHAR(45) NOT NULL,
  `Nivel_educativo` VARCHAR(45) NOT NULL,
  `Servicio_educativo` VARCHAR(45) NOT NULL,
  `Turno` VARCHAR(45) NOT NULL,
  `Ambito` VARCHAR(45) NOT NULL,
  `Control_c` VARCHAR(45) NOT NULL,
  `Domicilio` VARCHAR(80) NOT NULL,
  `Num_exterior` VARCHAR(25) NOT NULL,
  `Entre_calle` VARCHAR(85) NOT NULL,
  `Y_calle` VARCHAR(45) NOT NULL,
  `Calle_posterior` VARCHAR(45) NULL DEFAULT NULL,
  `Codigo_postal` SMALLINT NULL DEFAULT NULL,
  `Telefono` INT NULL DEFAULT NULL,
  `Correo_elec` VARCHAR(100) NULL DEFAULT NULL,
  `Lada` SMALLINT NULL DEFAULT NULL,
  `Altitud_msnm` VARCHAR(25) NOT NULL,
  `Longitud` VARCHAR(25) NOT NULL,
  `Latitud` VARCHAR(25) NOT NULL,
  `Longitug_gms` VARCHAR(25) NOT NULL,
  `Latitud_gms` VARCHAR(25) NOT NULL,
  `Periodo` VARCHAR(20) NOT NULL,
  `localidad_Id_Localidad` INT(10) NOT NULL,
  PRIMARY KEY (`Id_Escuela`, `localidad_Id_Localidad`),
  INDEX `fk_escuela_localidad1_idx` (`localidad_Id_Localidad` ASC) VISIBLE,
  CONSTRAINT `fk_escuela_localidad1`
    FOREIGN KEY (`localidad_Id_Localidad`)
    REFERENCES `escuela`.`localidad` (`Id_Localidad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `escuela`.`equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`equipo` (
  `Total_grupos` SMALLINT NOT NULL,
  `Aulas_existentes` SMALLINT NOT NULL,
  `Aulas_uso` SMALLINT NOT NULL,
  `Laboratorios` SMALLINT NOT NULL,
  `Cp_operativa` SMALLINT NOT NULL,
  `Talleres` SMALLINT NOT NULL,
  `Cp_internet` SMALLINT NOT NULL,
  `Cp_educativo` SMALLINT NOT NULL,
  `escuela_Id_Escuela` INT(10) NOT NULL,
  PRIMARY KEY (`escuela_Id_Escuela`),
  CONSTRAINT `fk_equipo_escuela`
    FOREIGN KEY (`escuela_Id_Escuela`)
    REFERENCES `escuela`.`escuela` (`Id_Escuela`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `escuela`.`personas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escuela`.`personas` (
  `Total_personal` INT NOT NULL,
  `Personal_mujer` TINYINT NOT NULL,
  `Personal_hombres` TINYINT NOT NULL,
  `Docentes` TINYINT NOT NULL,
  `DocentesMujeres` TINYINT NOT NULL,
  `DocentesHombres` TINYINT NOT NULL,
  `Alumnos` TINYINT NOT NULL,
  `AlumnosMujeres` TINYINT NOT NULL,
  `AlumnosHombres` TINYINT NOT NULL,
  `escuela_Id_Escuela` INT(10) NOT NULL,
  PRIMARY KEY (`escuela_Id_Escuela`),
  CONSTRAINT `fk_personas_escuela1`
    FOREIGN KEY (`escuela_Id_Escuela`)
    REFERENCES `escuela`.`escuela` (`Id_Escuela`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
